boto==2.38.0
Django==1.8.6
django-storages-redux==1.3
pytz==2015.7
requests==2.8.1
