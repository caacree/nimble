from django.db import models
from django.utils import timezone
import datetime

from django.contrib import admin
from .apps import *

# Create your models here.
class AppSessions(models.Model):
    name = models.CharField(max_length=90)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(default=datetime.datetime(2099, 1, 1))

    def save(self, *args, **kwargs):
        if self.id == None:
            self.start_time = timezone.now()
        super(AppSessions, self).save(*args, **kwargs)

    def app_data(self):
        if self.name == "temp":
            data = TempApp(self.start_time, self.end_time)
        elif self.name == "door":
            data = DoorOpen(self.start_time, self.end_time)
        elif self.name == "toothbrush":
            data = Toothbrush(self.start_time, self.end_time)
        return data.client_data()

    class Meta:
        ordering = ["-end_time"]

class SensorData(models.Model):
    # sensor data upload
    sensor = models.CharField(default="1", max_length=30)
    time = models.DateTimeField(default=timezone.now)
    # "key1"
    key1 = models.BooleanField(default=False)
    # "key2"
    key2 = models.BooleanField(default=False)
    # "AmbTemp" outside air temperature
    AmbTemp = models.FloatField(default=0)
    # "IRTemp" near surface temperature
    IRTemp = models.FloatField(default=0)
    # "humidity"
    humidity = models.FloatField(default=0)
    # "baroPres"
    baroPres = models.FloatField(default=0)
    # "baroHeight"
    baroHeight = models.FloatField(default=0)
    # "accX"
    accX = models.FloatField(default=0)
    # "accY"
    accY = models.FloatField(default=0)
    # "accZ"
    accZ = models.FloatField(default=0)
    # "gyroX"
    gyroX = models.FloatField(default=0)
    # "gyroY"
    gyroY = models.FloatField(default=0)
    # "gyroZ"
    gyroZ = models.FloatField(default=0)
    # "magX"
    magX = models.FloatField(default=0)
    # "magY"
    magY = models.FloatField(default=0)
    # "magZ"
    magZ = models.FloatField(default=0)
    # "optical"
    optical = models.FloatField(default=0)

    class Meta:
        ordering = ['time']

admin.site.register(AppSessions)
admin.site.register(SensorData)