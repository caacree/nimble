# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AppSessions',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=90)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField(default=datetime.datetime(2099, 1, 1, 0, 0))),
            ],
            options={
                'ordering': ['-end_time'],
            },
        ),
        migrations.CreateModel(
            name='SensorData',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('sensor', models.CharField(max_length=30, default='1')),
                ('time', models.DateTimeField(default=datetime.datetime(2015, 11, 22, 0, 32, 0, 884212, tzinfo=utc))),
                ('key1', models.BooleanField(default=False)),
                ('key2', models.BooleanField(default=False)),
                ('AmbTemp', models.FloatField(default=0)),
                ('IRTemp', models.FloatField(default=0)),
                ('humidity', models.FloatField(default=0)),
                ('baroPres', models.FloatField(default=0)),
                ('baroHeight', models.FloatField(default=0)),
                ('accX', models.FloatField(default=0)),
                ('accY', models.FloatField(default=0)),
                ('accZ', models.FloatField(default=0)),
                ('gyroX', models.FloatField(default=0)),
                ('gyroY', models.FloatField(default=0)),
                ('gyroZ', models.FloatField(default=0)),
                ('magX', models.FloatField(default=0)),
                ('magY', models.FloatField(default=0)),
                ('magZ', models.FloatField(default=0)),
                ('optical', models.FloatField(default=0)),
            ],
        ),
    ]
