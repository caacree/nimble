from django.views.generic import TemplateView
from django.http import HttpResponse
from datetime import datetime
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
import json
import pytz
from django.core.serializers.json import DjangoJSONEncoder

from .models import AppSessions, SensorData

# Create your views here.
class HomePage(TemplateView):
    template_name = "homepage.html"

    def get_order(self):
        recent = AppSessions.objects.all()[:10]
        order = []
        for item in recent:
            if item.name not in order and item.name in ["door", "temp"]:
                order.append(item.name)
        return order

    def get_context_data(self, *args, **kwargs):

        options = {"toothbrush": {"template": "appTemplates/toothbrush.html", "data": []},
         "door": {"template": "appTemplates/doorOpen.html", "data": AppSessions.objects.filter(name="door")[0].id},
         "temp": {"template" : "appTemplates/temperature.html", "data": AppSessions.objects.filter(name="temp")[0].id}}
        for item in AppSessions.objects.filter(name="toothbrush")[:5]:
            options["toothbrush"]["data"].append(item.id)
        order = self.get_order()
        objects = []
        for item in order:
            objects.append(options[item])
        context = super(HomePage, self).get_context_data(*args, **kwargs)
        context['apps'] = objects
        recent = AppSessions.objects.all()[0]
        print("thiese")
        print(recent.end_time)
        print(recent)
        print(recent.end_time > datetime(2098, 1, 1).replace(tzinfo=pytz.utc))
        if recent.end_time > datetime(2098, 1, 1).replace(tzinfo=pytz.utc):
            context["current"] = recent.name
            context["currentID"] = recent.id
        else:
            context["current"] = "None"
            context["currentID"] = "None"

        return context

@csrf_exempt
def sensorEndpoint(request):
    if request.method == 'POST':
        data = dict(request.POST.dict())

        datapoint = SensorData(**data)
        datapoint.save()
        #dj_image = request.FILES['image']
        #form = UploadImageForm(request.POST, request.FILES)

    response = HttpResponse()
    response["status_code"] = 200
    return response

@csrf_exempt
def appChange(request):
    if request.method == 'GET':
        try:
            current = AppSessions.objects.get(end_time=datetime(2099, 1, 1))
        except AppSessions.DoesNotExist:
            current = None

        new = request.GET.get("app")
        print("stuff here")
        print(current)
        print(new)
        if current != None:
            if current.name == new:
                return HttpResponse('')
            current.end_time = timezone.now()
            current.save()

        if new != "None":
            newapp = AppSessions(name=new, start_time=timezone.now())
            newapp.save()
            return HttpResponse(newapp.id)

        return HttpResponse('None')

def getData(request):
    try:
        current = AppSessions.objects.get(id=request.GET.get('id'))
    except AppSessions.DoesNotExist:
        current = None
        return HttpResponse('')

    return HttpResponse(json.dumps(current.app_data(), cls=DjangoJSONEncoder))


