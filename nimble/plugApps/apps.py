from django.apps import apps
import math
import random
import json

class SensorApp(object):
    def __init__(self, start_time=1, end_time=1):
        self.start = start_time
        self.end = end_time
        SensorData = apps.get_model('plugApps', 'SensorData')
        self.data = SensorData.objects.filter(time__gte=start_time, time__lte=end_time)

    def client_data(self):
        # this needs to be implemented in each app
        return None

class Toothbrush(SensorApp):

    def client_data(self):
        response_data = []

        def calc_median(samples):
            samples_sorted = sorted(samples)
            return samples_sorted[2]

        last_samples = []
        started_brushing = False

        for sample in self.data:
            mag_accel_x = pow(float(sample.accX), 2)
            mag_accel_y = pow(float(sample.accY), 2)
            mag_accel_z = pow(float(sample.accZ), 2)
            g = math.fabs(math.sqrt(mag_accel_x + mag_accel_y + mag_accel_z) - 0.23)
            last_samples.append(g)

            if len(last_samples) == 5:
                median = calc_median(last_samples)
                last_samples.pop(0)

                if median > 0.1:
                    if started_brushing:
                        # Nothing to do.
                        pass
                    else:
                        last_start_time = sample.time
                        started_brushing = True
                else:
                    if started_brushing:
                        duration = sample.time - last_start_time
                        print("we've appended")
                        response_data.append({'duration': duration, 'start_time': last_start_time})
                        started_brushing = False
                    else:
                        # Nothing to do
                        pass
        if started_brushing:
            duration = sample.time - last_start_time
            response_data.append({'duration': duration, 'start_time': last_start_time})

        response_data = {"time": self.start, "length":sum([item["duration"] for item in response_data])}
        #response_data = {"time": random.random()*150}
        return response_data


class DoorOpen(SensorApp):
    def client_data(self):
        response_data = []

        for sample in self.data:
            mag_magnetometer_x = pow(float(sample.magX), 2)
            mag_magnetometer_y = pow(float(sample.magY), 2)
            mag_magnetometer_z = pow(float(sample.magZ), 2)
            mag_magnetometer = math.sqrt(mag_magnetometer_x + mag_magnetometer_y + mag_magnetometer_z)
            if mag_magnetometer < 300:
                response_data.append({'status': 1, 'time': sample.time})
            else:
                response_data.append({'status': 0, 'time': sample.time})
        return response_data


class TempApp(SensorApp):

    def client_data(self):

        response_data = []
        for sample in self.data:
            response = {'temp': sample.IRTemp, 'time': sample.time}
            if sample.IRTemp > 30:
                response['alert'] = True
            else:
                response['alert'] = False
            response_data.append(response)
        return response_data