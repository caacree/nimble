from storages.backends.s3boto import S3BotoStorage

"""Custom S3 storage backend to store files in subfolders"""
MediaRootS3BotoStorage = lambda: S3BotoStorage(location="nimble/media")
StaticRootS3BotoStorage = lambda: S3BotoStorage(location="nimble/static")